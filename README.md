# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here

Canvas左邊區塊:可讓直線筆、矩形筆、圓形筆選擇不同顏色。

Canvas右邊區塊:水藍色區塊裡共有八種功能，分別是:
    1.直線筆:鉛筆圖案，按住滑鼠能畫出直線。
    2.橡皮擦:橡皮擦圖案，能擦掉Canvas上的任何圖案，包含字。
    3.矩形筆:正方形圖案，按住滑鼠並拖曳可以畫出矩形。
    4.三角形筆:三角形圖案，按住滑鼠並拖曳可以畫出三角形。
    5.圓形筆:圓形圖案，按住滑鼠並拖曳可以畫出圓形。
    6.Reset:紅色圓形按鈕，能清除掉Canvas上所有畫過的痕跡。
    7.彩虹筆:彩虹圖案，按住滑鼠並拖曳，能隨著拖曳長度隨機畫出不同顏色的直線。
    8.下載:箭頭往下圖案，能下載目前Canvas。
    點擊1~7後皆能讓cursor變不同樣式。

    接下來是水藍色區塊以下，由上而下分別是:
    1.長條拖曳，能改變筆和字的粗細。
    2.左邊第一個list點擊後可以選擇字型，右邊的list可以選擇字的size。
    若已選擇一次後想再改選擇別的字型或大小，必須把list的字全刪除掉才可選擇。
    3.點擊Canvas上任意一點後，按下選擇檔案，可以上傳圖片至Canvas指定位置。

Canvas下邊區塊:
    1.Redo:箭頭往左，可以回到上一步。
    2.Undo:箭頭往右，可以到下一步。
    3.打字:首先在空白格裡打上想打的字，打完先點擊Canvas上任何一點後再按下Enter就可以讓字印在Canvas上。