var canvas = document.getElementById('Paint');
var ctx = canvas.getContext('2d');
var mode = 0;
var word_x, word_y;
var rectInit_x, rectInit_y, rect_flag = 0;
var triInit_x, triInit_y, tri_flag = 0;
var cirInit_x, cirInit_y, cir_flag = 0;
var rainbow_flag = 0;
var r = 0, g = 0, b = 0;
var r_flag = 0, g_flag = 0, b_flag = 0;

let state = ctx.getImageData(0, 0, canvas.width, canvas.height);
window.history.pushState(state, null);
ctx.font = '90px serif';


function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    };
}

function mouseMove(evt) {
    var mousePos = getMousePos(canvas, evt);
    textPos = mousePos;
    if (mode == 0 || mode == 5){
        ctx.globalCompositeOperation="source-over";
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.stroke();
    }
    else if (mode == 1 && rect_flag == 2){
        ctx.globalCompositeOperation="source-over";
        ctx.fillRect(rectInit_x, rectInit_y, mousePos.x - rectInit_x, mousePos.y - rectInit_y);
    }
    else if (mode == 2 && tri_flag == 2){
        ctx.globalCompositeOperation="source-over";
        ctx.moveTo(triInit_x, triInit_y);
        ctx.lineTo(mousePos.x, mousePos.y);
        ctx.lineTo(triInit_x - (mousePos.x - triInit_x), mousePos.y);
        ctx.fill();
    }
    else if (mode == 3 && cir_flag == 2){
        ctx.globalCompositeOperation="source-over";
        ctx.arc(cirInit_x,cirInit_y,mousePos.x - cirInit_x,0,Math.PI*2,false);
        ctx.fill();
    }
    else if (mode == 4){
        ctx.globalCompositeOperation="destination-out";
        ctx.arc(mousePos.x,mousePos.y,8,0,Math.PI*2,false);
        ctx.fill();
    }
    if (mode == 5){
        r = (r + 1)%256;
        g = (g + 3)%256;
        b = (b + 5)%256;
        ctx.strokeStyle = 'rgb(' + r + ',' + g + ',' + b + ')';
    }
}

canvas.addEventListener('mousedown', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    ctx.beginPath();
    if (mode == 0)
        ctx.moveTo(mousePos.x, mousePos.y);
    evt.preventDefault();
    if (mode == 1 && rect_flag == 1){
        ctx.moveTo(mousePos.x, mousePos.y);
        rectInit_x = mousePos.x;
        rectInit_y = mousePos.y;
        rect_flag = 2;
    }
    else if (mode == 2 && tri_flag == 1){
        triInit_x = mousePos.x;
        triInit_y = mousePos.y;
        tri_flag = 2;
    }
    else if (mode == 3 && cir_flag == 1){
        cirInit_x = mousePos.x;
        cirInit_y = mousePos.y;
        cir_flag = 2;
    }
    canvas.addEventListener('mousemove', mouseMove, false);
});

canvas.addEventListener('mouseup', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    if (mousePos.x < 800 && mousePos.y < 600){
        word_x = mousePos.x;
        word_y = mousePos.y;
    }
    if (rect_flag == 2){
        rect_flag = 1;
    }
    if (tri_flag == 2){
        tri_flag = 1;
    }
    if (cir_flag == 2){
        cir_flag = 1;
    }
    var state = ctx.getImageData(0, 0, canvas.width, canvas.height);
    window.history.pushState(state, null);

    canvas.removeEventListener('mousemove', mouseMove, false);
}, false);

var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black', 'white', 'ebebeb'];

function Brush(i){
    if (i == 0){
        mode = 0;
        modeBackground(0);
        document.body.style.cursor = 'col-resize';
    }
    else if (i == 1){
        mode = 1;
        rect_flag = 1;
        modeBackground(2);
        document.body.style.cursor = 'crosshair';
    }
    else if (i == 2){
        mode = 2;
        tri_flag = 1;
        modeBackground(3);
        document.body.style.cursor = 'context-menu';
    }
    else if (i == 3){
        mode = 3;
        cir_flag = 1;
        modeBackground(4);
        document.body.style.cursor = 'all-scroll';
    }
}

function Color(i){
    if (mode == 0){
        mode = 0;
        ctx.strokeStyle = colors[i];
        document.body.style.cursor = 'move';
    }
    else{
        ctx.fillStyle = colors[i];
    }
}

var l = document.getElementById('line');
l.style.backgroundColor = 'pink';

function modeBackground(i){
    var l = document.getElementById('line');
    var e = document.getElementById('erase');
    var r = document.getElementById('rect');
    var t = document.getElementById('tri');
    var c = document.getElementById('cir');
    var rain = document.getElementById('rainbow');
    if (i == 0){
        l.style.backgroundColor = 'pink';
        e.style.backgroundColor = 'skyblue';
        r.style.backgroundColor = 'skyblue';
        t.style.backgroundColor = 'skyblue';
        c.style.backgroundColor = 'skyblue';
        rain.style.backgroundColor = 'skyblue';
    }
    if (i == 1){
        l.style.backgroundColor = 'skyblue';
        e.style.backgroundColor = 'pink';
        r.style.backgroundColor = 'skyblue';
        t.style.backgroundColor = 'skyblue';
        c.style.backgroundColor = 'skyblue';
        rain.style.backgroundColor = 'skyblue';
    }
    if (i == 2){
        l.style.backgroundColor = 'skyblue';
        e.style.backgroundColor = 'skyblue';
        r.style.backgroundColor = 'pink';
        t.style.backgroundColor = 'skyblue';
        c.style.backgroundColor = 'skyblue';
        rain.style.backgroundColor = 'skyblue';
    }
    if (i == 3){
        l.style.backgroundColor = 'skyblue';
        e.style.backgroundColor = 'skyblue';
        r.style.backgroundColor = 'skyblue';
        t.style.backgroundColor = 'pink';
        c.style.backgroundColor = 'skyblue';
        rain.style.backgroundColor = 'skyblue';
    }
    if (i == 4){
        l.style.backgroundColor = 'skyblue';
        e.style.backgroundColor = 'skyblue';
        r.style.backgroundColor = 'skyblue';
        t.style.backgroundColor = 'skyblue';
        c.style.backgroundColor = 'pink';
        rain.style.backgroundColor = 'skyblue';
    }
    if (i == 5){
        l.style.backgroundColor = 'skyblue';
        e.style.backgroundColor = 'skyblue';
        r.style.backgroundColor = 'skyblue';
        t.style.backgroundColor = 'skyblue';
        c.style.backgroundColor = 'skyblue';
        rain.style.backgroundColor = 'pink';
    }
}

function Rainbow() {
    document.body.style.cursor = 'cell';
    mode = 5;
    modeBackground(5);
}

function fontSizes(){
    var size = document.getElementById('Size');
    ctx.lineWidth = size.value;
}

function reset(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function erase(){
    mode = 4;
    document.body.style.cursor = 'pointer';
    modeBackground(1);
}


function writeWord(){
    var word = document.getElementById('word');
    ctx.strokeText(word.value, word_x, word_y);   
}

var type = document.getElementById('Type');
var Tsize = document.getElementById('TSize');
type.addEventListener('change', Text, false);
Tsize.addEventListener('change', Text, false);

function Text(){
    if (type.value == 1){
        ctx.font = Tsize.value + 'px serif';
        type.innerHTML = "serif";
    }
    else if (type.value == 2){
        ctx.font = Tsize.value + 'px monospace';
        type.innerHTML = "monospace";
    }
    else if (type.value == 3){
        ctx.font = Tsize.value + 'px cursive';
        type.innerHTML = "cursive";
    }
    else if (type.value == 4){
        ctx.font = Tsize.value + 'px fantasy';
        type.innerHTML = "fantasy";
    }
    Tsize.innerHTML = Tsize.value;
}

download_img = function(el){
    var image = canvas.toDataURL("image/jpg");
    el.href = image;
    console.log("FFF");
};

window.addEventListener('popstate', changeStep, false);

function changeStep(e){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    console.log(e.state);
    if (e.state){
        console.log("NNNN");
        ctx.putImageData(e.state, 0, 0);
    }
    console.log("VVVV");
}

function ReUnDo(i){
    window.history.go(i);
}

var uploadImage = document.getElementById('upload');
uploadImage.addEventListener('change', Upload, false);
function Upload(e){
    var reader = new FileReader();
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img, word_x, word_y);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}
